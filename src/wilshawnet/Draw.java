package wilshawnet;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import javax.swing.JPanel;

/**
 *
 * @author paul
 */
public class Draw extends JPanel{
    
    private final static int ITEMSIZE = 32;
    private final static int MARGIN = 16; 
    private final static int OFFSET = 32;
        
    private boolean[] inputNeurons;
    private boolean[] trainingNeurons;
    private boolean[] outputNeurons;
    private boolean[][] neurons;
    
    private int threshold = 0;
    
    private int patternsPresented;
    private int mi; // Nodes turned on in input patterns
    private int mo; // Nodes turned on in ouput patterns
    private double probabilityConectionIsActive;
    private double errorProbability;
    
    public Draw(){}
    
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        this.setBackground(Color.DARK_GRAY);        
        g.setColor(Color.YELLOW);
        
        if(inputNeurons == null || trainingNeurons == null || outputNeurons == null || neurons == null){return;}        
        
        g.drawString("Threshold: " + threshold, OFFSET/2, OFFSET/2);
        
        // Display Input neurons
        for(int i = 0; i < inputNeurons.length; i++){
            
            g.drawLine(OFFSET + ITEMSIZE, (i*ITEMSIZE) + (i*MARGIN) + (ITEMSIZE*2) + OFFSET,
                    (ITEMSIZE*outputNeurons.length) + (MARGIN*outputNeurons.length) + ITEMSIZE + OFFSET,
                    (i*ITEMSIZE) + (i*MARGIN) + (ITEMSIZE*2) + OFFSET);
            
            int ovalY = (OFFSET + ITEMSIZE + (i*ITEMSIZE) + (i*MARGIN)) + (ITEMSIZE/2);
            
            if(inputNeurons[i]){
                g.fillOval(OFFSET, ovalY , ITEMSIZE , ITEMSIZE);
            }else{
                g.drawOval(OFFSET, ovalY , ITEMSIZE , ITEMSIZE);
            }
            
            for(int s = 0; s < neurons[i].length; s++){                
                int xPos = (s*ITEMSIZE) + (s*MARGIN) + (ITEMSIZE*2) + OFFSET;
                
                // Synapses
                int[] synTriX = {xPos + (ITEMSIZE/2) + (ITEMSIZE/4), xPos + ITEMSIZE - (ITEMSIZE/8) , xPos + ITEMSIZE - (ITEMSIZE/8)};
                int[] synTriY = {OFFSET + (ITEMSIZE*2) + (i*ITEMSIZE) + (i*MARGIN) + (ITEMSIZE/2),
                    OFFSET + (ITEMSIZE*2) + (i*ITEMSIZE) + (i*MARGIN) + (ITEMSIZE/4), 
                    OFFSET + (ITEMSIZE*2) + (i*ITEMSIZE) + (i*MARGIN) + (ITEMSIZE/2) + (ITEMSIZE/4)};
                int synTriN = 3;
                Polygon synapseTriangle = new Polygon(synTriX,synTriY,synTriN);
                g.drawLine(xPos + (ITEMSIZE/2), OFFSET + (ITEMSIZE*2) + (i*ITEMSIZE) + (i*MARGIN),
                        xPos + (ITEMSIZE/2), OFFSET + (ITEMSIZE*2) + (i*ITEMSIZE) + (i*MARGIN) + (ITEMSIZE/2));
                g.drawLine(xPos + (ITEMSIZE/2), OFFSET + (ITEMSIZE*2) + (i*ITEMSIZE) + (i*MARGIN) + (ITEMSIZE/2),
                            xPos + (ITEMSIZE/2) + (ITEMSIZE/4), OFFSET + (ITEMSIZE*2) + (i*ITEMSIZE) + (i*MARGIN) + (ITEMSIZE/2));
                
                if(neurons[i][s]){
                    g.drawPolygon(synapseTriangle);
                    g.fillPolygon(synapseTriangle);
                }else{
                    g.drawPolygon(synapseTriangle);
                }
            }
        }
    
        
        // Training neurons
        for(int t = 0; t < trainingNeurons.length; t++){
            // Training Signal
            int ovalX = (ITEMSIZE*2) + (t*ITEMSIZE) + (t*MARGIN) + OFFSET;
            
            //Axon
            g.drawLine(ovalX + (ITEMSIZE/2), OFFSET + ITEMSIZE, ovalX + (ITEMSIZE/2), OFFSET + ITEMSIZE + (ITEMSIZE/2));
            g.drawLine(ovalX + (ITEMSIZE/2), OFFSET + ITEMSIZE + (ITEMSIZE/2), ovalX + (ITEMSIZE/2) + (ITEMSIZE/4), OFFSET + ITEMSIZE + (ITEMSIZE/2));
            
            // Synapse
            int[] synTriX = {ovalX + (ITEMSIZE/2) + (ITEMSIZE/4), ovalX + ITEMSIZE - (ITEMSIZE/8) , ovalX + ITEMSIZE - (ITEMSIZE/8)};
            int[] synTriY = {OFFSET + ITEMSIZE + (ITEMSIZE/2), OFFSET + ITEMSIZE + (ITEMSIZE/4), OFFSET + ITEMSIZE + (ITEMSIZE/2) + (ITEMSIZE/4)};
            int synTriN = 3;
            Polygon synapseTriangle = new Polygon(synTriX,synTriY,synTriN);
            
            if(trainingNeurons[t]){
                g.fillOval(ovalX, OFFSET,ITEMSIZE, ITEMSIZE);
                g.drawPolygon(synapseTriangle);
                g.fillPolygon(synapseTriangle);
            }else{
                g.drawOval(ovalX, OFFSET, ITEMSIZE, ITEMSIZE);
                g.drawPolygon(synapseTriangle);
            }
        }
        
        
        // Output neurons
        for(int o  = 0; o < outputNeurons.length; o++){
            g.drawLine((ITEMSIZE*2) + (o*ITEMSIZE) + (o*MARGIN) + ITEMSIZE + OFFSET, OFFSET + ITEMSIZE,
                    (ITEMSIZE*2) + (o*ITEMSIZE) + (o*MARGIN) + ITEMSIZE + OFFSET, (inputNeurons.length * ITEMSIZE) + (inputNeurons.length * MARGIN) + (ITEMSIZE*2) + OFFSET);
            
            int ovalX = (ITEMSIZE*2) + (o*ITEMSIZE) - (ITEMSIZE / 2) + (o*MARGIN) + OFFSET + ITEMSIZE;
            int ovalY = (inputNeurons.length * ITEMSIZE) + (inputNeurons.length * MARGIN) + (ITEMSIZE*2) + OFFSET;
            
            if(outputNeurons[o]){
                g.fillOval(ovalX,ovalY,ITEMSIZE, ITEMSIZE);
            }else{
                g.drawOval(ovalX, ovalY, ITEMSIZE, ITEMSIZE);
            }
        }
        
        // Advanced info area
        int xAfterNetwork = (ITEMSIZE*(outputNeurons.length+3)) + (MARGIN*outputNeurons.length) + OFFSET;
        g.drawString("Patterns Presented: " + patternsPresented, xAfterNetwork, OFFSET);
        g.drawString("Mi (Nodes on in input patterns): " + mi, xAfterNetwork, OFFSET + 20);
        g.drawString("Mo (Nodes on in output patterns): " + mo, xAfterNetwork, OFFSET + 40);
        g.drawString("Prob Connection Is Turned On: " + probabilityConectionIsActive, xAfterNetwork, OFFSET + 60);
        g.drawString("Error Probability: " + errorProbability, xAfterNetwork, OFFSET + 80);
        
    }

    public static int getITEMSIZE() {
        return ITEMSIZE;
    }

    public static int getMARGIN() {
        return MARGIN;
    }

    public static int getOFFSET() {
        return OFFSET;
    }
    
    public void setInputNeurons(boolean[] inputNeurons) {
        this.inputNeurons = inputNeurons;
    }

    public void setTrainingNeurons(boolean[] trainingNeurons) {
        this.trainingNeurons = trainingNeurons;
    }

    public void setOutputNeurons(boolean[] outputNeurons) {
        this.outputNeurons = outputNeurons;
    }

    public void setNeurons(boolean[][] neurons) {
        this.neurons = neurons;
    }
    
    public void setThreshold(int threshold){
        this.threshold = threshold;
    }

    public void setPatternsPresented(int patternsPresented) {
        this.patternsPresented = patternsPresented;
    }

    public void setMi(int mi) {
        this.mi = mi;
    }

    public void setMo(int mo) {
        this.mo = mo;
    }

    public void setProbabilityConectionIsActive(double probabilityConectionIsActive) {
        this.probabilityConectionIsActive = probabilityConectionIsActive;
    }

    public void setErrorProbability(double errorProbability) {
        this.errorProbability = errorProbability;
    }
}

