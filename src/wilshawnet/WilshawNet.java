package wilshawnet;

import java.lang.reflect.InvocationTargetException;
import javax.swing.SwingUtilities;

/**
 *
 * @author paul
 */
public class WilshawNet {

    private final boolean[] inputNeurons;
    private final boolean[] trainingNeurons;
    private final boolean[] outputNeurons;
    private final boolean[][] neurons;
    
    private int threshold;
    
    private int patternsPresented;
    private int mi; // Nodes turned on in input patterns
    private int mo; // Nodes turned on in ouput patterns
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GUI gui = new GUI(); //start the gui in a swing thread
        try{            
            SwingUtilities.invokeAndWait((Runnable) gui);
        }catch (InterruptedException ex){
            System.out.println("Probelms loading the GUI in the background! " + ex.getMessage());
        }catch (InvocationTargetException ex){
           System.out.println("Probelms loading the GUI in the background! " + ex.getMessage());
        }
    }
    
    public WilshawNet(int inputSize, int outputSize){
        threshold = 1;
        
        patternsPresented = 0;
        mi = 0;
        mo = 0;
        
        inputNeurons = new boolean[inputSize];
        for(int i = 0; i < inputSize; i++){
            inputNeurons[i] = false;
        }
        
        trainingNeurons = new boolean[outputSize];
        for(int o = 0; o < outputSize; o++){
            trainingNeurons[o] = false;
        }
        
        outputNeurons = new boolean[outputSize];
        for(int o = 0; o < outputSize; o++){
            outputNeurons[o] = false;
        }
        
        neurons = new boolean[inputSize][outputSize];
        for(int i = 0; i < inputSize; i++){
            for(int o = 0; o < outputSize; o++){
                neurons[i][o] = false;
            }
        }
    }
    
    public void trainSignalToggled(int position){
        trainingNeurons[position] = !trainingNeurons[position];
        checkOutputs();
    }
    
    public void inputSignalToggled(int position){
        inputNeurons[position] = !inputNeurons[position];
        checkOutputs();
    }
    
    private void checkOutputs(){
        // Are any output neurons now activated
        for(int o = 0; o < outputNeurons.length; o++){
            int sumActivationsForOutput = 0;
            for(int n = 0; n < neurons.length; n++){
                if(neurons[n][o]){
                    if(inputNeurons[n]){
                        sumActivationsForOutput++;
                    }
                }else if(trainingNeurons[o]){
                    sumActivationsForOutput++;
                }
            }
            outputNeurons[o] = sumActivationsForOutput >= threshold;
        }
    }
    
    public void presentPattern(){
        // Check that some there is both a training signal and input
        boolean inputPresent = false;
        boolean trainSignalPresent = false;
        for(int i = 0; i < inputNeurons.length; i++){
            if(inputNeurons[i]){
                inputPresent = true;
                break;
            }
        }
        for(int t = 0; t < trainingNeurons.length; t++){
            if(trainingNeurons[t]){
                trainSignalPresent = true;
                break;
            }
        }
        if(!inputPresent || !trainSignalPresent){return;} // No input or train signal nothing to do
        
        // Increment number of presented patterns
        patternsPresented++;
        
        // Count input neurons on
        for(int i = 0; i < inputNeurons.length; i ++){
            if(inputNeurons[i]){
                mi++;
            }
        }
        
        // Count output neurons on
        for(int o = 0; o < outputNeurons.length; o++){
            if(outputNeurons[o]){
                mo++;
            }
        }
        
        // Learn by activating neurons
        for(int i = 0; i < inputNeurons.length; i++){
            for(int t = 0; t < trainingNeurons.length; t++){
                if(inputNeurons[i] && trainingNeurons[t]){
                    neurons[i][t] = true;
                }
            }
        }
        clearInputTrainNeurons();
    }
    
    private void clearInputTrainNeurons(){
        for(int i = 0; i < inputNeurons.length; i++){inputNeurons[i] = false;}
        for(int t = 0; t < trainingNeurons.length; t++){trainingNeurons[t] = false;}
        for(int o = 0; o < outputNeurons.length; o++){outputNeurons[o] = false;}
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
        checkOutputs();
    }
    
    public int getThreshold() {
        return threshold;
    }
    
    public int getPatternsPresented() {
        return patternsPresented;
    }

    public int getMi() {
        return mi;
    }

    public int getMo() {
        return mo;
    }
    
    public double getP(){
        // probabilityConectionIsActive
        double ni = inputNeurons.length;  // double to induce double division below
        double no = outputNeurons.length;
        return 1.0 - ((1.0 - ((mi*mo) / (ni*no))) * (1.0 - ((mi*mo) / (ni*no))));
    }
    
    public double getErrorProbability(){
        return Math.pow(getP(), mi);
    }
    
    public boolean[] getInputNeurons() {
        return inputNeurons;
    }

    public boolean[] getTrainingNeurons() {
        return trainingNeurons;
    }

    public boolean[] getOutputNeurons() {
        return outputNeurons;
    }

    public boolean[][] getNeurons() {
        return neurons;
    }
}
