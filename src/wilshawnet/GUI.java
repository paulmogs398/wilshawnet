package wilshawnet;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Observable;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

/**
 *
 * @author paul
 */
public class GUI extends Observable implements Runnable{
    private WilshawNet  wilshawNet;
    
    private final JFrame mainWindow = new JFrame("Wilshaw Associative Network");
    private Draw d;
    
    //popup menu
    private JPopupMenu drawMenu = new JPopupMenu();
    private JMenuItem NNItem = new JMenuItem("New Network");
    private JMenuItem PPItem = new JMenuItem("Present Pattern");
    private JMenuItem STItem = new JMenuItem("Set Threshold");
 
    public GUI(){
        mainWindow.setSize(900,800);
        mainWindow.setResizable(true);
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindow.setLayout(new BorderLayout());
        d = new Draw();
        d.addMouseListener(new mouseEvent());
        d.addMouseMotionListener(new mouseEvent());
        mainWindow.add(d, BorderLayout.CENTER);

        drawMenu.add(NNItem);
        NNItem.addActionListener(new actionEvent());
        drawMenu.add(PPItem);
        PPItem.addActionListener(new actionEvent());
        drawMenu.add(STItem);
        STItem.addActionListener(new actionEvent());
        d.setComponentPopupMenu(drawMenu);

        mainWindow.setVisible(true);
    }
    
    @Override
    public void run(){};  //necessary for EDT technology
        
    /**
     * An action listener for responsing to button presses
     */
    public class actionEvent implements ActionListener{
        @Override
        public void actionPerformed (ActionEvent e){
            //drop down menu options         
            if(e.getSource()==NNItem){newNetworkItem();}
            if(e.getSource()==PPItem){presentPatternItem();}
            if(e.getSource()==STItem){setThresholdItem();}
        }
    }
    
    public class mouseEvent implements MouseListener, MouseMotionListener{
        // Mouse Listener
        @Override
        public void mouseClicked(MouseEvent me) {
            if(me.getButton()==MouseEvent.BUTTON1){
                System.out.println("X: " + me.getX() + " Y:" + me.getY());
                processClick(me.getX(),me.getY());
            }
        }
        @Override
        public void mousePressed(MouseEvent me){}
        @Override
        public void mouseReleased(MouseEvent me) {}
        @Override
        public void mouseEntered(MouseEvent me) {}
        @Override
        public void mouseExited(MouseEvent me) {}
        
        // Mouse Motion Listener
        @Override
        public void mouseDragged(MouseEvent me){}
        @Override
        public void mouseMoved(MouseEvent me) {}
    }
    
    private void updateGui(){
        d.setInputNeurons(wilshawNet.getInputNeurons());
        d.setTrainingNeurons(wilshawNet.getTrainingNeurons());
        d.setOutputNeurons(wilshawNet.getOutputNeurons());
        d.setNeurons(wilshawNet.getNeurons());
        d.setThreshold(wilshawNet.getThreshold());
        d.setMi(wilshawNet.getMi());
        d.setMo(wilshawNet.getMo());
        d.setPatternsPresented(wilshawNet.getPatternsPresented());
        d.setProbabilityConectionIsActive(wilshawNet.getP());
        d.setErrorProbability(wilshawNet.getErrorProbability());
        
        d.repaint();
        mainWindow.invalidate();
        mainWindow.repaint();
        mainWindow.validate();
    }
    
    private void newNetworkItem(){
        String inputSizeStr = (String)JOptionPane.showInputDialog("Please enter number of input neurons");
        int inputSize = Integer.parseInt(inputSizeStr);
        String outputSizeStr = (String)JOptionPane.showInputDialog("Please enter number of output neurons");        
        int outputSize = Integer.parseInt(outputSizeStr);
        wilshawNet = new WilshawNet(inputSize,outputSize);
        
        updateGui();
    }
    
    private void presentPatternItem(){
        if(wilshawNet == null){
            JOptionPane.showMessageDialog(mainWindow,"Please create a network first.");
            return;
        }
        wilshawNet.presentPattern();
        updateGui();
    }
    
    private void setThresholdItem(){
        if(wilshawNet == null){
            JOptionPane.showMessageDialog(mainWindow,"Please create a network first.");
            return;
        }
        String thresholdStr = (String)JOptionPane.showInputDialog("Please enter a threshold");
        int threshold = 0;
        try{
            threshold = Integer.parseInt(thresholdStr);
        }catch(NumberFormatException nfe){
            JOptionPane.showMessageDialog(mainWindow,"Please enter a integer number for threshold");
            return;
        }
        wilshawNet.setThreshold(threshold);
        updateGui();
    }
    
    private void processClick(int clickX, int clickY){
        if(wilshawNet == null){return;}
        
        // Is the click on a input neuron?
        if(clickX > Draw.getOFFSET() && clickX < (Draw.getOFFSET() + Draw.getITEMSIZE())){
            // Which input neuron?
            for(int i = 0; i < wilshawNet.getInputNeurons().length; i++){ 
                if(clickY >  (Draw.getITEMSIZE()/2) + Draw.getITEMSIZE() + (i*Draw.getITEMSIZE()) + (i*Draw.getMARGIN()) + Draw.getOFFSET() &&
                   clickY <  (Draw.getITEMSIZE()/2) + (Draw.getITEMSIZE()*2) + (i*Draw.getITEMSIZE()) + (i*Draw.getMARGIN()) + Draw.getOFFSET()){
                    wilshawNet.inputSignalToggled(i);
                    updateGui();
                    return;
                }
            }
        }
        // Is the click on a training neuron?
        for(int t = 0; t < wilshawNet.getTrainingNeurons().length; t++){
            if(clickY > (Draw.getOFFSET()) && clickY < (Draw.getOFFSET() + Draw.getITEMSIZE())){
                if(clickX >  (Draw.getITEMSIZE()*2) + (t*Draw.getITEMSIZE()) + (t*Draw.getMARGIN()) + Draw.getOFFSET() &&
                   clickX <  (Draw.getITEMSIZE()*3) + (t*Draw.getITEMSIZE()) + (t*Draw.getMARGIN()) + Draw.getOFFSET()){
                    wilshawNet.trainSignalToggled(t);
                    updateGui();
                    return;
                }
            }
        }
    }
    
}

